/*--------------------
 * Document Ready
 *--------------------*/
$(function() {
  $(document).ready();

  if (typeof init == "function") init(); // me == ლ(ಠ_ಠლ)
});

/*--------------------
 * Show data
 *--------------------*/
function init() {
  // get data
}

/**
 * Request to API
 * @param {string} method GET, POST
 * @param {string} url link to api -> /api/name
 */
function makeRequest(method, url) {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();

    request.open(method, url);
    request.onload = resolve;
    request.onerror = reject;
    request.send();
  });
}