var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'TMS',
  description: 'NodeJS Reifenprogramm Server',
  script: 'C:\vaio\projektewanne\TMSTest\server.js',
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=4096'
  ]
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();

// Cleaning Up: Uninstall a Service
// Uninstalling a previously created service is syntactically similar to installation.

// var Service = require('node-windows').Service;

// // Create a new service object
// var svc = new Service({
//   name:'Hello World',
//   script: require('path').join(__dirname,'helloworld.js')
// });

// // Listen for the "uninstall" event so we know when it's done.
// svc.on('uninstall',function(){
//   console.log('Uninstall complete.');
//   console.log('The service exists: ',svc.exists);
// });

// // Uninstall the service.
// svc.uninstall();