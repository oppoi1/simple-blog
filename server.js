// server.js

// set up ======================================================================
// get all the tools we need
const express  		= require('express'),
	  session  		= require('express-session'),
	  opts 			= {logFilePath: 'output/project.log', timestamFormat: 'YYYY-MM-DD HH:mm:ss'},
 	  cookieParser 	= require('cookie-parser'),
 	  bodyParser 	= require('body-parser'),
 	  morgan 		= require('morgan'),
 	  app      		= express(),
 	  port     		= process.env.PORT || 8080,
 	  pool    		= require('./app/db'),
 	  log     		= require('simple-node-logger').createSimpleFileLogger(opts),
 	  passport 		= require('passport'),
 	  flash    		= require('connect-flash');

// configuration ===============================================================
// connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(express.static(__dirname + "/public")); // set public dir
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
	secret: 'vidyapathaisalwaysrunning',
	resave: true,
	saveUninitialized: true
 } )); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// create global user
app.use((req, res, next) => {
	res.locals.currentUser = req.user;
	res.locals.error = req.flash("error");
	res.locals.success = req.flash("success");
	next();
});

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
