// app/routes.js

module.exports = function(app, passport) {
  const express = require("express"),
    opts = {
      logFilePath: "output/api.log",
      timestamFormat: "YYYY-MM-DD HH:mm:ss"
    };
  (pool = require("./db")),
    (mysql = require("mysql")),
    (log = require("simple-node-logger").createSimpleFileLogger(opts));

  log.setLevel("warn");
  // =====================================
  // API AND DATABASE CONNECTION =========
  // =====================================

  /* GET home page. */
  app.get("/", function(req, res) {
    res.render("index", {
      title: "Simple Blog"
    });
    let newIp = req.connection._peername.address.replace(/^.*:/, ""); // save ip from user?
  });

  /* GET user login/signup page. */
  app.get("/user", function(req, res) {
    res.render("user.ejs"); // load user.ejs file
  });

  /* GET data. */


  /* UPDATE data. */

  // =====================================
  // LOGIN ===============================
  // =====================================
  // show the login form
  app.get("/login", function(req, res) {
    // render the page and pass in any flash data if it exists
    res.render("login.ejs", { message: req.flash("loginMessage") });
  });

  // process the login form
  app.post(
    "/login",
    passport.authenticate("local-login", {
      successRedirect: "/profile", // redirect to the secure profile section
      failureRedirect: "/login", // redirect back to the signup page if there is an error
      failureFlash: true // allow flash messages
    }),
    function(req, res) {
      if (req.body.remember) {
        req.session.cookie.maxAge = 1000 * 60 * 3;
      } else {
        req.session.cookie.expires = false;
      }
      req.flash("success", "Willkommen");
      res.redirect("/");
    }
  );

  // =====================================
  // SIGNUP ==============================
  // =====================================
  // show the signup form
  app.get("/signup", function(req, res) {
    // render the page and pass in any flash data if it exists
    res.render("signup.ejs", { message: req.flash("signupMessage") });
  });

  // process the signup form
  app.post(
    "/signup",
    passport.authenticate("local-signup", {
      successRedirect: "/profile", // redirect to the secure profile section
      failureRedirect: "/signup", // redirect back to the signup page if there is an error
      failureFlash: true // allow flash messages
    })
  );

  // =====================================
  // PROFILE SECTION =====================
  // =====================================
  // we will want this protected so you have to be logged in to visit
  // we will use route middleware to verify this (the isLoggedIn function)
  app.get("/profile", isLoggedIn, function(req, res) {
    res.render("profile.ejs", {
      user: req.user, // get the user out of session and pass to template
      message: req.flash("noLoginMessage")
    });
  });

  // =====================================
  // LOGOUT ==============================
  // =====================================
  app.get("/logout", isLoggedIn, function(req, res) {
    req.logout();
    req.flash("success", "Erfolgreich ausgeloggt.");
    res.redirect("/");
  });

  /**
   * MIDDLEWARE
   *
   * @param req  // not necessary
   * @param res  // not necessary
   * @param next // not necessary
   * @returns next() // logs user in and grants permission
   */
  function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) return next();

    // if they aren't, redirect them to the home PAGE
    req.flash("error", "Sie müssen sich einloggen um diese Seite zu sehen.");
    res.redirect("/");
  }
}